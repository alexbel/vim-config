call plug#begin('~/.vim/bundle')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins repos
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'AndrewRadev/splitjoin.vim'
Plug 'FooSoft/vim-argwrap'
Plug 'Lokaltog/vim-easymotion'
Plug 'Yggdroot/indentLine'
Plug 'alexbel/vim-rubygems'
Plug 'alexbel/vim-whisper-theme'
Plug 'beloglazov/vim-online-thesaurus'
Plug 'ntpeters/vim-better-whitespace'
Plug 'chrisbra/NrrwRgn'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'elzr/vim-json'
Plug 'ervandew/supertab'
Plug 'godlygeek/tabular'
Plug 'gorkunov/smartpairs.vim'
Plug 'hashivim/vim-terraform'
Plug 'haya14busa/vim-auto-programming'
Plug 'int3/vim-extradite'
Plug 'janx/vim-rubytest'
Plug 'jiangmiao/auto-pairs'
Plug 'jlanzarotta/bufexplorer'
Plug 'jszakmeister/vim-togglecursor'
Plug 'junegunn/vim-easy-align'
Plug 'ludovicchabant/vim-gutentags'
Plug 'mattn/gist-vim'
Plug 'mattn/webapi-vim'
Plug 'mephux/vim-jsfmt'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'mileszs/ack.vim'
Plug 'mmozuras/vim-github-comment'
Plug 'morhetz/gruvbox'
Plug 'posva/vim-vue', { 'for': 'vue' }
Plug 'rhysd/git-messenger.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'slm-lang/vim-slm'
Plug 'szw/vim-dict'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'vim-scripts/IndexedSearch'
Plug 'vim-scripts/JSON.vim'
Plug 'vim-scripts/vimwiki'

" syntax
Plug 'pangloss/vim-javascript'
Plug 'slim-template/vim-slim'
Plug 'smancill/conky-syntax.vim'
Plug 'vim-ruby/vim-ruby'

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General behaviour
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible
set modelines=0
set nomodeline
" Look ahead as search pattern is specified
set incsearch
" Ignore case
set ignorecase
"...unless uppercase letters used
set smartcase

"Highlight all matches
"set hlsearch
" don't wait ESC-sequences
set ttimeoutlen=50
" more commands history
set history=100
" ... and more undolevels
set undolevels=2048
" reload buffer when external changes detected
set autoread
" allow backspace in insert mode
set backspace=2
" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif
" Open new window at the bottom
set splitbelow

set clipboard=unnamed,exclude:.*
set clipboard=unnamedplus

" fold based on indent
set foldmethod=manual
" don't fold by default
set nofoldenable

" Indent settings
" ---------------
set tabstop=2
set shiftwidth=2
" Default to 2 spaces as tabs
set softtabstop=2
" replace tabs by spaces
set expandtab
" Enable syntax coloring only for short lines
set synmaxcol=300

" Backup and swap settings
" ------------------------
set backup
set backupdir=~/tmp,/tmp " store backups
set dir=~/tmp,/tmp       " files in RAM


" Filetypes and encoding
" ----------------------
" utf-8 by default
set encoding=utf-8
" the order of enumeration encoding
set fileencodings=utf-8,iso-8859-1,iso-8859-5,windows-1251,koi8-r
" spell
set spelllang=en_us,ru


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User interface setings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax on

" more colors
set t_Co=256

" colors and theme overrides
set cursorline
let g:gruvbox_italic=0
let g:gruvbox_underline=0
colorscheme gruvbox
set background=dark

hi Boolean      ctermfg=73
hi ColorColumn  ctermbg=234
hi Constant     ctermfg=73
hi CursorLineNr ctermfg=223 ctermbg=237
hi Directory    ctermfg=107
hi Function     ctermfg=173 ctermbg=None
hi LineNr       ctermfg=243 ctermbg=237
hi Normal       ctermbg=None
hi Number       ctermfg=73
hi String       ctermfg=107
hi Type         ctermfg=179
hi Typedef      ctermfg=179

hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red

"vertical/horizontal scroll off settings
set scrolloff=3
set sidescrolloff=7
set sidescroll=1

" always show statusline
set laststatus=2

" show non-printing characters
"set list listchars=tab:··,trail:·,extends:»,precedes:«

" show line numbers
set number
set numberwidth=3
" set indentation as previous line
set autoindent
" Turn on autoindenting of blocks
set smartindent
" Show name of buffer in terminal title
set title

set lazyredraw

" show incomplete cmds down the bottom
set showcmd
" search and highlighting for brackets
set showmatch

set matchpairs+=<:>
" use wildmenu
set wildmenu
set wildcharm=<TAB>
" wrap long lines
set wrap

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vars
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let javascript_fold = 0
let mapleader       = ","
let g:mapleader     = ","
" disable matchparens
let loaded_matchparen = 1
" disable underline, bold, italic styling for html files
let html_no_rendering = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins settings and plugins mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" vim-auto-programming
set completefunc=autoprogramming#complete

" Syntastic
let g:syntastic_aggregate_errors       = 1
let g:syntastic_javascript_checkers    = ['jshint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'
let g:syntastic_scss_checkers          = ['stylelint']
let g:syntastic_markdown_checkers      = ['proselint']
let g:syntastic_auto_loc_list          = 1
let g:syntastic_loc_list_height        = 3
let g:syntastic_enable_highlighting    = 0
let g:syntastic_error_symbol           = '●'
let g:syntastic_style_error_symbol     = '●'
let g:syntastic_warning_symbol         = '!'
let g:syntastic_style_warning_symbol   = '!'
let g:syntastic_css_csslint_args       = "--ignore= adjoining-classes,floats,font-faces,shorthand,font-sizes,regex-selectors,important"
" syntastic can't find rvm version of ruby
let g:syntastic_ruby_mri_exec = $HOME.'/.rvm/rubies/ruby-3.0.2/bin/ruby'
let g:syntastic_ruby_mri_args = "-wc"
" NERDTree
let g:NERDTreeWinSize           = 40
let g:NERDTreeChristmasTree     = 1
let g:NERDTreeCaseSensitiveSort = 1
let g:NERDTreeQuitOnOpen        = 0
let g:NERDTreeWinPos            = 'left' "by default
let g:NERDTreeShowBookmarks     = 1
let g:NERDTreeDirArrows         = 0

" vim-json
let g:vim_json_syntax_conceal = 0

" git-messenger
let g:git_messenger_include_diff = 'current'
let g:git_messenger_max_popup_height = 20
let g:git_messenger_close_on_cursor_moved = 'false'

" ctrlp
set wildignore+=*/tmp/*,*.zip,*.gz,tags
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
let g:ctrlp_map = 's'

" Ack
" grep word under cursor in current directory
nnoremap <Leader><Leader>g :Ack <cword><cr>
if executable('rg')
  let g:ackprg = 'rg --ignore-case --line-number'
endif

" vim-rubytests
let g:rubytest_output = "terminal"
let g:rubytest_cmd_spec     = "bundle exec spring rspec %p"
let g:rubytest_cmd_example  = "bundle exec spring rspec %p:'%c'"

map <F3>      <Plug>RubyTestRun
map <Leader>f <Plug>RubyFileRun

" gist-vim
let g:gist_clip_command = 'xclip -selection clipboard'
let g:gist_post_private = 1

" vimwiki
let g:vimwiki_list = [{'path': '~/.vimwiki/'}]

" Surround
" remap surround key mappings
" t+s(ingle) quotes, t+d(ouble) quotes
nmap ts csw'El
nmap td csw"El
nmap <leader>- yss-
nmap <leader>= yss=

" jdaddy
nnoremap <Leader>j :exe jdaddy#reformat('jdaddy#outer_pos', v:count1)<cr>

" indentLine
let g:indentLine_color_term = 236
let g:indentLine_concealcursor = ''

" vim-airline
let g:airline#extensions#branch#enabled = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_left_sep          = ''
let g:airline_right_sep         = ''
let g:airline_symbols.linenr    = '¶'
let g:airline_symbols.branch    = '⎇'
let g:airline_symbols.paste     = '⇲'
let g:airline_theme             = 'whisper'
let g:airline_symbols.space     = ' '
let g:airline_symbols.maxlinenr = ''

" emmet
let g:user_emmet_expandabbr_key = '<c-e>'

" vim-online-thesaurus
let g:online_thesaurus_map_keys = 0
nnoremap <Leader><Leader>r :OnlineThesaurusCurrentWord<CR>

" vim-jsfmt
"let g:js_fmt_autosave = 1

" togglecursor
let g:togglecursor_default = 'block'

" vim-argwrap
nnoremap <silent> <leader>a :ArgWrap<CR>

" vim-javascript
let g:javascript_conceal_function = 'ƒ'

" vim-gutentags
let g:gutentags_ctags_tagfile = '.git/tags'

" supertab
let g:SuperTabCompletionContexts = ['MyTagContext']


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Commands and mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Easy window navigation
noremap <space>h <C-w>h
noremap <space>j <C-w>j
noremap <space>k <C-w>k
noremap <space>l <C-w>l

command! Vimrc e ~/.vim/vimrc

" close quickfix window
nmap <silent> <leader>q :ccl<CR>

" Key mappings
map               <F1> <C-]>
imap              <F1> <Esc><C-]>
nmap              <F2> <C-^>
nmap     <silent> <F8> :BufExplorer<CR>
map      <silent> <F9> :wall<CR>:call FormatAll()<CR> :w<CR>
nmap              <F10>       :NERDTreeToggle<cr>
vmap              <F10>  <esc>:NERDTreeToggle<cr>i
imap              <F10>  <esc>:NERDTreeToggle<cr>i

" copy & paste to system clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" switch to the last file
nnoremap <leader><leader>l <c-^>

" write as sudo
cnoremap w!! w !sudo tee % >/dev/null

" Visual Block mode is far more useful that Visual mode (so swap the commands)...
nnoremap v <C-V>
nnoremap <C-V> v

vnoremap v <C-V>
vnoremap <C-V> v

" resize window CTRL+(h|j|k|l)
noremap <C-j> :resize +1<CR>
noremap <C-k> :resize -1<CR>
noremap <C-h> :vertical resize -1<CR>
noremap <C-l> :vertical resize +1<CR>

" swap : ;
nnoremap ; :

" remap esc
inoremap jj <Esc>

" Make vaa select the entire file...
vmap aa VGo1G

" When shifting, retain selection over multiple shifts
vmap <expr> > KeepVisualSelection(">")
vmap <expr> < KeepVisualSelection("<")

"
" Autocommands
"
augroup general
  " remove all existing autocmds
  autocmd!

  " reload .vimrc after saving
  au BufWritePost $MYVIMRC source $MYVIMRC

  " Save if focus lost
  au BufLeave,FocusLost * silent! wall

  au BufRead,BufNewFile *.coffee set ft=coffee syntax=coffee
  au BufRead,BufNewFile *.scss set ft=scss syntax=scss
  au BufRead,BufNewFile /etc/nginx/* set ft=nginx
  au BufRead,BufNewFile *.json set ft=json
  au BufNewFile,BufRead *.markdown,*.md,*.mdown,*.mkd,*.mkdn set ft=markdown
  au BufRead,BufNewFile Vagrantfile set ft=ruby

  " Make .sh files executable on write
  au BufWritePost *.sh silent !chmod a+x %

  " Restore cursor position
  au BufReadPost *   if line("'\"") > 1 && line("'\"") <= line("$") |
                   \   exe "normal! g`\"" |
                   \ endif

  " hide cursorline when focus is on other window
  au VimEnter,WinEnter,BufwinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline

  " open nerdtree when vim starts
  au VimEnter * if !argc() && &filetype != 'man' | NERDTree | endif

  " open csv files in read-only by default
  au vimenter *.csv set ro

  au FileType markdown setlocal spell
  au BufRead,BufNewFile *.md,*.txt,*.wiki setlocal spell

augroup END

" Enable filetype detection
filetype plugin on
filetype plugin indent on
