## My vim settings

### Requirements:
* Vim 7.3+
* Python support for Vim, Python 2.4+

optional:
* Exuberant ctags 5.5
* NodeJS, npm (to install csslint,jsonlint,jshint and etc.)
* ripgrep
* csslint
* jshint
* jsonlint
* js-yaml
* jsfmt


## Screenshots
![screenshot](screenshots/snapshot1.png)

### Installation (archlinux):

    sudo pacman -S gvim vim-runtime ctags nodejs npm ripgrep

    sudo npm install jshint -g
    sudo npm install jsonlint -g
    sudo npm install stylelint stylelint-config-standard-scss -g
    sudo npm install js-yaml -g
    sudo npm install jsfmt -g

Another distributives:
You can install these packages via your favorite package manager or install from source code

Clone repo:

    git clone git@gitlab.com:alexbel/vim-config.git ~/.vim

and run:

    . .vim/install.sh

to install environment.
Then open vim and install plugins


    :BundleInstall

